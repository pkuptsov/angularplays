  'use strict';
  var myApp = angular.module('myApp', ['ngRoute']);

      myApp.config(function($routeProvider) {
        $routeProvider.
          when('/', {
            templateUrl: '../templates/model-list.html',
            controller: 'ModelListCtrl'
          }).
          when('/:modelId', {
            templateUrl: '../templates/model-details.html',
            controller: 'ModelDetailCtrl'
          }).
          otherwise({
            redirectTo: '/'
          });
      });

       myApp.factory('models', function($http){
        return {
          list: function (callback){
            $http({
              method: 'GET',
              url: '/models.json',
              cache: true
            }).success(callback);
          },
          find: function(id, callback){
            $http({
              method: 'GET',
              url: '/models/' + id + '.json',
              cache: true
            }).success(callback);
          }
        };
      });
       
      myApp.controller('ModelListCtrl', function ($scope, models){
        models.list(function(models) {
          $scope.models = models;
        });
        
        
      });

      myApp.controller('ModelDetailCtrl', function ($scope, $routeParams, models){
        models.find($routeParams.modelId, function(model) {
          $scope.model = model;
        });
      });
 
// form tested      
function UserForm($scope, $http) {
    
  var master = {
    name: 'Имя',
    image: '/images/leister_storage_case_300dpi.jpeg'
  };
   
  $scope.cancel = function() {
    $scope.form = angular.copy(master);
  };
  
$scope.save = function() {    
    master = $scope.form;
    
    $http({
              method: 'POST',
              url: '/save',
              cache: true,
              data: {
                        name: master.name,
                        image: master.image,
                    },
             //headers:  {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'}
            }).success(function( res ){
                if (res.status == 'ok') {
                    //$scope.models.push([$scope.res.id);
                    alert('Добавлено id: ' + res.id);
                } else {
                    alert('Возникла ошибка');
                }
            }).error(function(err){
                    alert(err);
    });
     
    $scope.cancel();
  };
 
  $scope.isCancelDisabled = function() {
    return angular.equals(master, $scope.form);
  };
 
  $scope.isSaveDisabled = function() {
    return $scope.myForm.$invalid || angular.equals(master, $scope.form);
  };
 
  $scope.cancel();
}