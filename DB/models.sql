﻿-- Table: models

-- DROP TABLE models;

CREATE TABLE models
(
  id serial NOT NULL,
  name character varying(100) NOT NULL,
  image character varying(100),
  CONSTRAINT pk_models PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE models
  OWNER TO sri;
